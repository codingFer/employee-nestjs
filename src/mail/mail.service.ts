import { Injectable } from '@nestjs/common';
import { MailerService } from "@nestjs-modules/mailer";

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {
  }

  async sendUserCreate() {
    await this.mailerService.sendMail({
      to: 'codingFer@gmail.com',
      subject: 'Welcome slave :D',
      text: 'This is an example text'
    });
  }
}
