import { Module } from '@nestjs/common';
import { MailService } from './mail.service';
import { MailerModule } from "@nestjs-modules/mailer";

@Module({
  imports: [
    MailerModule.forRoot({
      transport: {
        host: 'smtp.gmail.com',
        port: 587,
        secure: false,
        auth: {
          user: 'silviaptest@gmail.com',
          pass: '28Agosto'
        }
      },
      defaults: {
        from: '"No Reply" <noreply@example.com>',
      },
    })
  ],
  providers: [MailService],
  exports: [MailService]
})
export class MailModule {}
