import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from "@nestjs/config";
import { EmployeeModule } from "./employee/employee.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { MailModule } from './mail/mail.module';

@Module({
  imports: [EmployeeModule, ConfigModule.forRoot({
    envFilePath: '.env'
  }), TypeOrmModule.forRoot({
    type: 'sqlite',
    database: 'db',
    entities: [__dirname + '/**/*.entity{.ts,.js}'],
    synchronize: true,
  }), MailModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
