import { Body, Controller, Delete, Get, Param, Patch, Post, Put } from "@nestjs/common";
import { EmployeeService } from "./employee.service";
import { Employee } from "../employee.entity";
import { CommandBus } from "@nestjs/cqrs";
import { CreateEmployeeCommand } from "../commands/create-employee/create-employee.command";
import { CreateEmployeeDto } from "../dto/create-employee.dto";

@Controller('employees')
export class EmployeeController {

  constructor(private employeeService: EmployeeService,
              private readonly commandBus: CommandBus) {
  }

  @Get()
  get(): Promise<Employee[]> {
    return this.employeeService.findAll();
  }

  @Get(':id')
  async getById(@Param('id')id: number): Promise<any> {
    return this.employeeService.findById(id);
  }

  @Post()
  async create(@Body() employeeData: CreateEmployeeDto): Promise<any> {
    return this.commandBus.execute(new CreateEmployeeCommand(employeeData))
  }

  @Patch(':id')
  async update(@Param('id')id: number,
               @Body() employeeData: Employee): Promise<any> {
    employeeData.id = id;
    console.log('update #' + employeeData.id);
    return this.employeeService.path(employeeData);
  }

  @Delete(':id')
  async delete(@Param('id')id: number): Promise<any> {
    return this.employeeService.delete(id);
  }
}
