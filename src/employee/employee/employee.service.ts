import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { Employee } from "../employee.entity";
import { DeleteResult, Repository, UpdateResult } from "typeorm";
import { MailService } from "../../mail/mail.service";

@Injectable()
export class EmployeeService {
  constructor(
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
  ) {
  }

  async findAll(): Promise<Employee[]> {
    return await this.employeeRepository.find();
  }

  async findById(id: number): Promise<Employee> {
    return await this.employeeRepository.findOne(id);
  }

  async post(employee: Employee): Promise<Employee> {
    return await this.employeeRepository.save(employee);
  }

  async path(employee: Employee): Promise<UpdateResult> {
    return await this.employeeRepository.update(employee.id ,employee);
  }

  async delete(id: number): Promise<DeleteResult> {
    return await this.employeeRepository.delete(id);
  }
}
