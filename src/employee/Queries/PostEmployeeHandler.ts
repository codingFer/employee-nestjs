import { IQueryHandler, QueryHandler } from "@nestjs/cqrs";
import { PostEmployeeQuery } from "./PostEmployeeQuery";
import { Repository } from "typeorm";
import { Employee } from "../employee.entity";
import { EmployeeRepository } from "../repository/employee-repository";

@QueryHandler(PostEmployeeQuery)
export class PostEmployeeHandler implements IQueryHandler<PostEmployeeQuery> {
  constructor(private employeeRepository: EmployeeRepository) {
  }

  execute(query: PostEmployeeQuery): Promise<Employee> {
    return this.employeeRepository.create(query.employee);
  }

}
