import { IQuery } from "@nestjs/cqrs";
import { Employee } from "../employee.entity";

export class PostEmployeeQuery implements IQuery {
  constructor(public employee: Employee) {
  }
}
