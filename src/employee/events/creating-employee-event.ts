import { CreateEmployeeDto } from "../dto/create-employee.dto";

export class CreatingEmployeeEvent {
  constructor(public readonly employee : CreateEmployeeDto) {
  }
}
