import { EventsHandler, IEventHandler } from "@nestjs/cqrs";
import { CreatingEmployeeEvent } from "./creating-employee-event";
import { EmployeeRepository } from "../repository/employee-repository";
import { MailService } from "../../mail/mail.service";

@EventsHandler(CreatingEmployeeEvent)
export class CreatingEmployeeHandler implements IEventHandler<CreatingEmployeeEvent>{
  constructor(private repository: EmployeeRepository,
              private mailService: MailService) {
  }

  async handle(event: CreatingEmployeeEvent): Promise<any> {
    console.log('D:');
    await this.mailService.sendUserCreate();
    return this.repository.create(event.employee);
  }

}
