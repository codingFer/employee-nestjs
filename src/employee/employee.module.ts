import { Module } from "@nestjs/common";
import { EmployeeService } from "./employee/employee.service";
import { EmployeeController } from "./employee/employee.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Employee } from "./employee.entity";
import { CqrsModule } from "@nestjs/cqrs";
import { CommandsHandlers } from "./commands";
import { EmployeeRepository } from "./repository/employee-repository";
import { EmployeeRepositoryImpl } from "./repository/employee-repository-impl";
import { MailModule } from "../mail/mail.module";
import { CreatingEmployeeHandler } from "./events/creating-employee.handler";

@Module({
  imports: [
    TypeOrmModule.forFeature([Employee]),
    CqrsModule,
    MailModule
  ],
  providers: [{
    provide: EmployeeRepository,
    useClass: EmployeeRepositoryImpl
  },EmployeeService,
    ...CommandsHandlers,
    CreatingEmployeeHandler],
  controllers: [EmployeeController]
})
export class EmployeeModule {}
