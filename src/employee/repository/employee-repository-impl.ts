import { EmployeeRepository } from "./employee-repository";
import { CreateEmployeeDto } from "../dto/create-employee.dto";
import { Employee } from "../employee.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import axios from "axios";

export class EmployeeRepositoryImpl extends EmployeeRepository{

  constructor(@InjectRepository(Employee)
              private employeeRepository: Repository<Employee>) {
    super();
  }

  async create(payload: CreateEmployeeDto): Promise<Employee> {
    await axios.post('http://localhost:3001/contacts/addcontact', { firstName: payload.firstName, lastName: payload.lastName});
    return this.employeeRepository.save(payload);
  }

}
