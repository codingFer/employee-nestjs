import { Employee } from "../employee.entity";
import { CreateEmployeeDto } from "../dto/create-employee.dto";

export abstract class EmployeeRepository {
  abstract create(payload: CreateEmployeeDto): Promise<Employee>;
}
