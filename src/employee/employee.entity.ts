import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { AggregateRoot } from "@nestjs/cqrs";
import { CreatingEmployeeEvent } from "./events/creating-employee-event";
import { CreateEmployeeDto } from "./dto/create-employee.dto";

@Entity()
export class Employee extends AggregateRoot{
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  middleName: string;

  @Column()
  mail: string;

  @Column()
  dob: Date;

  @Column()
  ssn: string;

  create(employee: CreateEmployeeDto) {
    //logic
    console.log(':´v');
    this.apply(new CreatingEmployeeEvent(employee));
  }
}
