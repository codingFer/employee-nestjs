export interface CreateEmployeeDto {
  firstName: string;
  lastName: string;
  middleName: string;
  mail: string;
  dob: Date;
}
