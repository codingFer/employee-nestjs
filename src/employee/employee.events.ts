export class EmployeeEvents {
  constructor(public readonly orderTransactionGUID: string,
              public readonly orderUser: string,
              public readonly orderItem: string,
              public readonly employeeAmount: string,) {
  }
}
