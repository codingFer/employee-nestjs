import { CommandHandler, EventPublisher, ICommandHandler } from "@nestjs/cqrs";
import { CreateEmployeeCommand } from "./create-employee.command";
import { EmployeeRepository } from "../../repository/employee-repository";
import { Employee } from "../../employee.entity";

@CommandHandler(CreateEmployeeCommand)
export class CreateEmployeeHandler implements ICommandHandler<CreateEmployeeCommand> {
  constructor(private employeeRepository: EmployeeRepository) {
  }

  async execute(command: CreateEmployeeCommand) {
    const employee: Employee = new Employee();
    console.log('this is an test');
    employee.create(command.payload);
    employee.commit();
    return this.employeeRepository.create(command.payload);
  }

}
